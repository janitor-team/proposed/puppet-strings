==============
puppet-strings
==============

---------------------------------------
Generate Puppet documentation with YARD
---------------------------------------

:Author: Louis-Philippe Véronneau
:Date: 2019
:Manual section: 1

Synopsis
========

| puppet strings *<action>*
| puppet strings **--help**

Description
===========

Produce complete, user-friendly module documentation by using Puppet Strings.
Strings uses tags and code comments, along with the source code, to generate
documentation for a module's classes, defined types, functions, tasks, plans,
and resource types and providers.

Puppet Strings is based on the YARD Ruby documentation tool.

Actions
=======

| **generate**
|     Generate documentation from files.

|

| **server**
|     Runs a local documentation server for the modules in the current Puppet
|     environment.

Options
=======

| **--format** *FORMAT*  The rendering format to use (html, markdown or json)
| **--out**            The output file when using the markdown or json formats
| **--verbose**        Whether to log verbosely
| **--debug**          Whether to log debug information
| **--help**           Show the help screen

Examples
========

If you want to generate a single reference file, use something like:

    $ puppet strings generate --format markdown --out REFERENCE.md

You can otherwise generate HTML documentation this way:

    $ puppet strings generate

Bugs
====

Bugs can be reported to your distribution's bug tracker or upstream
at https://tickets.puppetlabs.com/browse/PDOC.
